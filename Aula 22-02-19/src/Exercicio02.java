import java.util.Scanner;

public class Exercicio02 {
		
		static int a, b;
		static double div;
		static Scanner conta = new Scanner(System.in);
		
		public static double divisao(double x, double y) {
			return x/y;
		}
		
		public static int divisao (int x, int y) {
			return x/y;
		}
	
	public static void main(String[] args) {
		
		a=0;
		b=0;
		div=0;
		
		System.out.println("Digite um valor: ");
		a= conta.nextInt();
		
		do {
			System.out.println("Digite outro valor: ");
			b= conta.nextInt();
		} while (b == 0);
		
		div = divisao(a, b);
		
		System.out.println("A divisao do primeiro valor com segundo eh: " + div);
		
		
		conta.close();
	}
	

}
