import java.util.Scanner;

public class App {
	
	static Scanner num = new Scanner(System.in);
	static int a;

	public static void main(String[] args) {
		
		System.out.println("Escolhe uma op��o: ");
		
		a= num.nextInt();
		
		switch (a) { 
			case 1: System.out.println("Pequeno"); break; 
			case 2: System.out.println("Medio"); break; 
			case 3: System.out.println("Grande"); break; 
			default: System.out.println("Nenhuma das respostas anteriores"); 
		}
		
	num.close();
	
	System.out.println("----For----");
	int i;
	for (i=0; i<10; i++)  
		System.out.println("i= " + i);
	
	System.out.println("----While----");
		
	i=0;
	while (i<10) {
		System.out.println("i= " + i);
		i++;
		
	}
	
	}
}
