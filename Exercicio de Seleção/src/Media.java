import java.util.Scanner;

/**
 * @author Victor
 * @version 1.5.1
 */
public class Media {
	
	static double nota1, nota2, media;
	static Scanner conta = new Scanner(System.in);
	
	public static double media_n() {
		return (nota1 + nota2)/2;
	}

	public static void main(String[] args) {
		
		System.out.println("Digite a nota da primeira prova: ");
		nota1= conta.nextDouble();
		System.out.println("Digite a nota da segunda prova: ");
		nota2= conta.nextDouble();
		
		media= media_n();
		
		if (media >= 6) {
			System.out.println("Parabens! Aprovado com " + media);
		}
		
	}

}
