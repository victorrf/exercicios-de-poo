
public class Questao4 {
	
	void meuMetodo(int x) {
		System.out.println("versao int");
	}
	
	void meuMetodo(double x) {
		System.out.println("versao Double");
	}

	public static void main(String[] args) {
		
		Questao4 OBJ = new Questao4();
		double x = 3;
		OBJ.meuMetodo(x);

	}

}
