package tse;

import java.util.Scanner;
import tse.Candidato;

public class APP {
    
    //Constante
    static final int MAXCONTA = 4;
   
    //vari�vel comum
    static int index = 0;
    static int votototal = 0;
    
    //Lista de contas
    static Candidato[] lista = new Candidato[MAXCONTA];
    
    static Scanner tecla = new Scanner(System.in);

    public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir Candidato");
            System.out.println("2-Votar");
            System.out.println("3-Apura��o");
            System.out.println("4-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: incluirCandidato(); break;
                case 2: votarCandidato(); break;
                case 3: apurarVotos(); break;    
                case 4: break;
            }
        } while (op!=4);       
    }
    
    
    public static void incluirCandidato(){
        //Entrada
        System.out.println("Digite o n�mero do candidato:");
        int num = tecla.nextInt();
        System.out.println("Digite o nome do candidato:");
        String nome = tecla.nextLine();
        //Criar o objeto e inserir na lista
        lista[index++] = new Candidato(num, nome);
        System.out.println("Candidato cadastrada com sucesso!");
    }
    
    public static void votarCandidato(){
        //Entrada
    	System.out.println("Digite o n�mero do candidato:");
        int num = tecla.nextInt();
        
        //Procurar a conta na lista
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getNumero()){
                lista[i].votarCandidato();
                break;
            }
        }
    }
    
    public static void apurarVotos(){;
        for (int i = 0; i < lista.length-1; i++) {
            if (lista[i] != null)
                System.out.println();
                
                //total += lista[i].getSaldo();
                total = total + lista[i].getSaldo();
            }else{
                break;
            }
        }
        System.out.println("Total:........" + total);
    }
  
}
