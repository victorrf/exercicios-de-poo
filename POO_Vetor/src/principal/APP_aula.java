package principal;
import tse.Conta;

public class APP_aula {

	public static void main(String[] args) {
		//Entrada
				Conta C1 = new Conta(12092, 23000);
				
				//Processamento
				C1.sacar(2000);
				
				//Saida
				System.out.println(C1.getSaldo());
	}

}
